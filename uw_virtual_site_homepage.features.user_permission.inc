<?php

/**
 * @file
 * uw_virtual_site_homepage.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_virtual_site_homepage_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_virtual_site_homepage content'.
  $permissions['create uw_virtual_site_homepage content'] = array(
    'name' => 'create uw_virtual_site_homepage content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'manager' => 'manager',
      'vsite admin' => 'vsite admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_virtual_site_homepage content'.
  $permissions['delete any uw_virtual_site_homepage content'] = array(
    'name' => 'delete any uw_virtual_site_homepage content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_virtual_site_homepage content'.
  $permissions['delete own uw_virtual_site_homepage content'] = array(
    'name' => 'delete own uw_virtual_site_homepage content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'manager' => 'manager',
      'vsite admin' => 'vsite admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_virtual_site_homepage content'.
  $permissions['edit any uw_virtual_site_homepage content'] = array(
    'name' => 'edit any uw_virtual_site_homepage content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'manager' => 'manager',
      'vsite admin' => 'vsite admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_virtual_site_homepage content'.
  $permissions['edit own uw_virtual_site_homepage content'] = array(
    'name' => 'edit own uw_virtual_site_homepage content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'manager' => 'manager',
      'vsite admin' => 'vsite admin',
    ),
    'module' => 'node',
  );

  return $permissions;
}
