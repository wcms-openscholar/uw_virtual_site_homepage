<?php

/**
 * @file
 * uw_virtual_site_homepage.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_virtual_site_homepage_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_uw_virtual_site_homepage';
  $strongarm->value = 0;
  $export['comment_anonymous_uw_virtual_site_homepage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_uw_virtual_site_homepage';
  $strongarm->value = 1;
  $export['comment_default_mode_uw_virtual_site_homepage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_uw_virtual_site_homepage';
  $strongarm->value = '50';
  $export['comment_default_per_page_uw_virtual_site_homepage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_uw_virtual_site_homepage';
  $strongarm->value = 1;
  $export['comment_form_location_uw_virtual_site_homepage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_uw_virtual_site_homepage';
  $strongarm->value = '1';
  $export['comment_preview_uw_virtual_site_homepage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_uw_virtual_site_homepage';
  $strongarm->value = 1;
  $export['comment_subject_field_uw_virtual_site_homepage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_uw_virtual_site_homepage';
  $strongarm->value = '2';
  $export['comment_uw_virtual_site_homepage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_uw_virtual_site_homepage';
  $strongarm->value = array(
    0 => 'cp',
  );
  $export['menu_options_uw_virtual_site_homepage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_uw_virtual_site_homepage';
  $strongarm->value = 'cp:390';
  $export['menu_parent_uw_virtual_site_homepage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_uw_virtual_site_homepage';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_uw_virtual_site_homepage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_uw_virtual_site_homepage';
  $strongarm->value = '1';
  $export['node_preview_uw_virtual_site_homepage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_uw_virtual_site_homepage';
  $strongarm->value = 1;
  $export['node_submitted_uw_virtual_site_homepage'] = $strongarm;

  return $export;
}
