<?php
/**
 * @file
 * uw_virtual_site_homepage.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_virtual_site_homepage_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_virtual_site_homepage_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function uw_virtual_site_homepage_image_default_styles() {
  $styles = array();

  // Exported image style: uw_homepage_banners_large.
  $styles['uw_homepage_banners_large'] = array(
    'label' => 'Homepage banners large',
    'effects' => array(
      5 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1024,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: uw_homepage_banners_medium.
  $styles['uw_homepage_banners_medium'] = array(
    'label' => 'Homepage banners medium',
    'effects' => array(
      3 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 769,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: uw_homepage_banners_small.
  $styles['uw_homepage_banners_small'] = array(
    'label' => 'Homepage banners small',
    'effects' => array(
      2 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 480,
          'height' => 250,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: uw_homepage_banners_xl.
  $styles['uw_homepage_banners_xl'] = array(
    'label' => 'Homepage banners xl',
    'effects' => array(
      1 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1280,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: uw_homepage_banners_xs.
  $styles['uw_homepage_banners_xs'] = array(
    'label' => 'Homepage banners xs',
    'effects' => array(
      4 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 320,
          'height' => 250,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_virtual_site_homepage_node_info() {
  $items = array(
    'uw_virtual_site_homepage' => array(
      'name' => t('UW Virtual Site Homepage'),
      'base' => 'node_content',
      'description' => t('University of Waterloo\'s Virtual Site homepage.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
