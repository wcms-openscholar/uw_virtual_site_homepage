<?php

/**
 * @file
 * uw_virtual_site_homepage.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_virtual_site_homepage_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_uw_twitter|node|uw_virtual_site_homepage|form';
  $field_group->group_name = 'group_uw_twitter';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_virtual_site_homepage';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Twitter',
    'weight' => '19',
    'children' => array(
      0 => 'field_uw_twitter_type',
      1 => 'field_uw_twitter_username',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_uw_twitter|node|uw_virtual_site_homepage|form'] = $field_group;

  return $export;
}
